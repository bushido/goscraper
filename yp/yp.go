package yp

import (
	"errors"
	"math"
	"net/url"
	"path"
	"regexp"
	"strconv"
	"strings"
	"time"

	"service/app/scraper"

	"github.com/PuerkitoBio/goquery"
	"github.com/metakeule/fmtdate"
)

const ypBaseURL = "http://www.yellowpages.com/listings/"

/*
	Selectors
*/

var (
	reviewCountClassSelector = "span[itemprop='reviewCount']"
	reviewClassSelector      = "div[itemprop='review']"
	idSelector               = "id"
	ratingClassSelector      = "meta[itemprop='ratingValue']"
	ratingContentSelector    = "content"
	authorSelector           = ".author"
	dateSelector             = ".date-posted"
	titleSelector            = ".review-title"
	textSelector             = ".review-response"
)

func isYPReviewURL(str string) bool {
	u, err := url.Parse(str)
	if err != nil {
		return false
	}
	reg := regexp.MustCompile("((www.)?(yp|yellowpages)\\.com)")
	if reg.MatchString(u.Host) {
		return true
	}
	return false
}

//MakeYPUrls generates pool of YP urls
func MakeYPUrls(baseURL string) (map[string]bool, error) {
	result := make(map[string]bool)
	if !isYPReviewURL(baseURL) {
		return result, errors.New("Invalid url")
	}
	res := make(chan int)
	errs := make(chan error)
	u, err := url.Parse(baseURL)
	if err != nil {
		return nil, err
	}
	s := strings.Split(path.Base(u.Path), "-")
	listingID := s[len(s)-1]

	go func() {
		doc, err := goquery.NewDocument(baseURL)
		if err != nil {
			errs <- err
			return
		}
		countReviews, err := strconv.Atoi(strings.Trim(doc.Find(reviewCountClassSelector).Text(), "()"))
		if err != nil {
			errs <- err
			return
		}
		pagesCount := int(math.Ceil(float64(countReviews) / 10.0))
		res <- pagesCount
	}()

	var cnt int
	select {
	case cnt = <-res:
		close(errs)
	case err = <-errs:
		return nil, err
	}
	if cnt > 0 {
		for i := 1; i <= cnt; i++ {
			str := ypBaseURL + listingID + "/reviews?page=" + strconv.Itoa(i)
			result[str] = true
		}
	}

	return result, nil

}

//ParseYP parses single url from YP
func ParseYP(res string) (*scraper.ParseResult, error) {
	if !isYPReviewURL(res) {
		return nil, errors.New("Invalid url")
	}
	doc, err := goquery.NewDocument(res)
	if err != nil {
		return nil, err
	}
	result := new(scraper.ParseResult)
	result.ParseInfo = make(map[int]map[string]string)
	doc.Find(reviewClassSelector).Each(func(i int, s *goquery.Selection) {
		var id, text, author, date, title string
		rating := "0"
		id, _ = s.Parent().Attr(idSelector)
		rating, _ = s.Find(ratingClassSelector).Attr(ratingContentSelector)
		author = s.Find(authorSelector).Text()
		date = strings.TrimSpace(s.Find(dateSelector).Text())
		dt, err := fmtdate.Parse("MM/DD/YYYY", date)
		if err != nil {
			date = time.Now().Format(time.RFC3339)
		} else {
			date = dt.Format(time.RFC3339)
		}
		title = s.Find(titleSelector).Text()
		text = strings.TrimSpace(s.Find(textSelector).Text())
		result.ParseInfo[i] = map[string]string{
			"id":     id,
			"text":   text,
			"author": author,
			"date":   date,
			"title":  title,
			"rating": rating,
		}

	})
	return result, nil

}
