package yp

import "testing"

func TestIsYPpUrl(t *testing.T) {
	t.Parallel()
	cases := []struct {
		in   string
		want bool
	}{
		{"bad url", false},
		{"http://www.yp.com/afafa", true},
		{"https://yp.com/afasff", true},
		{"https://www.yp.com/s", true},
		{"http://yp.com/afafa", true},
		{"http://yp.ca/afafa", false},
	}
	for _, c := range cases {
		got := isYPReviewURL(c.in)
		if got != c.want {
			t.Errorf("isYPReviewURL(%v) == %v, want %v", c.in, got, c.want)
		}
	}
}
