//Package scraper provides higher interface for scraping any website
package scraper

import (
	"sync"
	"time"
)

// A Target represents a target of scraping
type Target struct {
	BaseURL string
	URLPool map[string]bool
	Parser  func(str string) (*ParseResult, error)
	Saver   func(res *ParseResult) error
}

//A ParseResult represents universal structure for results of parser function
type ParseResult struct {
	ParseInfo map[int]map[string]string
}

// A Scraper represents scraper with all iternal information
type Scraper struct {
	//Batch represents amount of entities to scrape before sleep
	Batch int
	//BatchDelay represents sleep time after batch was scraped
	BatchDelay time.Duration

	//Delay to wait after each request to target host
	Delay time.Duration
	wg    *sync.WaitGroup
	mu    *sync.RWMutex

	DieOnSingleError bool

	//Pool of url's visited
	visited map[string]bool
}

// Scrape is helper function that combains save and parse routines for single url
func (target *Target) Scrape(url string) (err error) {
	res, err := target.Parser(url)

	if err != nil {
		return err
	}

	err = target.Saver(res)
	if err != nil {
		return err
	}

	return nil
}

//NewScraper initializes scraper with default values
func NewScraper() *Scraper {
	ret := new(Scraper)
	ret.Batch = 1000
	ret.Delay = time.Second / 2
	ret.BatchDelay = time.Second * 3
	ret.visited = make(map[string]bool)
	ret.DieOnSingleError = true
	var wg sync.WaitGroup
	var mu sync.RWMutex
	ret.wg = &wg
	ret.mu = &mu
	return ret
}

//Run starts scraping process. Returns error if any goroutine fired one
func (scraper *Scraper) Run(target *Target) error {
	var ind int
	errs := make(chan error, len(target.URLPool))
	finished := make(chan bool)
	for k := range target.URLPool {
		if ind < scraper.Batch {
			scraper.mu.Lock()
			_, ok := scraper.visited[k]
			scraper.mu.Unlock()
			if !ok {
				scraper.wg.Add(1)
				ind++
				go func(k string, scraper *Scraper, target *Target) {
					defer scraper.wg.Done()
					err := target.Scrape(k)
					if err != nil {
						errs <- err
					}
					scraper.mu.Lock()
					scraper.visited[k] = true
					scraper.mu.Unlock()
				}(k, scraper, target)
			}
		} else {
			time.Sleep(scraper.BatchDelay)
			ind = 0
		}
		time.Sleep(scraper.Delay)
	}

	go func() {
		scraper.wg.Wait()
		close(finished)
		close(errs)
	}()

	var err error
	if scraper.DieOnSingleError {
		select {
		case <-finished:
		case err = <-errs:
			if err != nil {
				return err
			}

		}
	} else {
		err = <-errs
		<-finished
	}

	if err != nil {
		return err
	}
	return nil

}
