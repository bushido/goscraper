// +build !race
package scraper

import (
	"errors"
	"log"
	"testing"
	"time"
)

const parseErrorResponse = "Some parse error"
const saveErrorResponse = "Some save error"

//Mocked parse functions to check for errors

func goodParser(any string) (*ParseResult, error) {
	res := &ParseResult{}
	return res, nil
}

func badParser(any string) (*ParseResult, error) {
	res := &ParseResult{}
	return res, errors.New("Some parse error")
}

func goodSaver(any *ParseResult) error {
	return nil
}

func badSaver(any *ParseResult) error {
	return errors.New("Some save error")
}

func TestScrape(t *testing.T) {
	t.Parallel()
	var err error
	target := new(Target)
	target.BaseURL = "http://someurl.com"
	target.URLPool = make(map[string]bool)

	target.Parser = badParser

	target.Saver = goodSaver

	err = target.Scrape("http://someurl.com")

	if err == nil || err.Error() != parseErrorResponse {
		t.Errorf("Should have failed with error \"%q\" but instead failed with \"%q\"", parseErrorResponse, err.Error())
	}

	target.Parser = goodParser

	target.Saver = badSaver

	err = target.Scrape("http://someurl.com")

	if err == nil || err.Error() != saveErrorResponse {
		t.Errorf("Should have failed with error \"%q\" but instead failed with \"%q\"", saveErrorResponse, err.Error())
	}
}

func TestRun(t *testing.T) {
	t.Parallel()
	var err error
	target := new(Target)
	target.BaseURL = "http://someurl.com"
	target.URLPool = make(map[string]bool)
	target.URLPool = map[string]bool{
		"http://exampleUrl1.com": false,
		"http://exampleUrl2.com": false,
	}
	scraper := NewScraper()

	target.Parser = badParser

	target.Saver = goodSaver

	err = scraper.Run(target)

	if err == nil || err.Error() != parseErrorResponse {
		t.Errorf("Should have failed with error \"%q\" but instead failed with \"%q\"", parseErrorResponse, err.Error())
	}
}

func TestDelayOptionDieImmediately(t *testing.T) {
	target := new(Target)
	target.URLPool = map[string]bool{
		"example":     true,
		"another one": true,
		"Fail":        true,
		"Don't fail":  true,
	}

	var checkRes []interface{}
	stdSaver := func(res *ParseResult) error {
		for _, v := range res.ParseInfo {
			log.Println(v)
		}
		return nil
	}

	tempParser := func(str string) (*ParseResult, error) {
		res := new(ParseResult)
		res.ParseInfo = make(map[int]map[string]string)
		time.Sleep(time.Second * 3)
		if str == "Fail" {
			checkRes = append(checkRes, errors.New("Haha failure"))
			return nil, errors.New("Haha failure")
		}

		res.ParseInfo[0] = map[string]string{
			"str": str,
		}
		checkRes = append(checkRes, str)
		return res, nil
	}

	target.Saver = stdSaver
	target.Parser = tempParser

	scraper := NewScraper()

	scraper.DieOnSingleError = true

	err := scraper.Run(target)

	if err == nil {
		t.Error("Expected error but didn't get it option die on error true")
	}
	foundErr := false
	for val := range checkRes {
		if _, ok := checkRes[val].(error); ok {
			foundErr = true
			break
		}
	}

	if !foundErr || len(checkRes) > 3 {
		t.Error("Expected not full resulst but got full, slice :%v", checkRes)
	}

}

func TestDelayOptionDontDieImmediately(t *testing.T) {
	target := new(Target)
	target.URLPool = map[string]bool{
		"example":     true,
		"another one": true,
		"Fail":        true,
		"Don't fail":  true,
	}
	var checkRes []interface{}

	stdSaver := func(res *ParseResult) error {
		for _, v := range res.ParseInfo {
			log.Println(v)
		}
		return nil
	}

	tempParser := func(str string) (*ParseResult, error) {
		res := new(ParseResult)
		res.ParseInfo = make(map[int]map[string]string)
		time.Sleep(time.Second * 3)
		if str == "Fail" {
			checkRes = append(checkRes, errors.New("Haha failure"))
			return nil, errors.New("Haha failure")
		}

		res.ParseInfo[0] = map[string]string{
			"str": str,
		}
		checkRes = append(checkRes, str)
		return res, nil
	}

	target.Saver = stdSaver
	target.Parser = tempParser

	scraper := NewScraper()

	scraper.DieOnSingleError = false

	err := scraper.Run(target)

	if err == nil {
		t.Error("Expected error but didn't get it in option die on error false")
	}

	for val := range checkRes {
		if checkRes[val] == nil {
			t.Errorf("Val should be error or string but it is %v", checkRes[val])
		}
	}

}
