// Example to show how YP scrape works
package main

import (
	"log"
	"runtime"
	"scrapper"
	"strconv"
	"strings"
	"time"

	"gopkg.in/mgo.v2"

	"github.com/user/scraper"
)

var IsDrop = true

// Helper struct to hold mongo collection
type ReviewsRepo struct {
	Coll *mgo.Collection
}

func (rp *ReviewsRepo) mongoSaver(res *scraper.ParseResult) error {
	bulk := reviewRepo.Coll.Bulk()
	bulk.Unordered()
	for _, v := range res.ParseInfo {
		date, _ := time.Parse(time.RFC3339, v["date"])
		rating, _ := strconv.ParseFloat(v["rating"], 64)
		bulk.Insert(&repos.Review{
			ReviewID: v["id"],
			Text:     v["text"],
			Title:    v["title"],
			Author:   v["author"],
			Link:     body.Link,
			Rating:   rating,
			Date:     &date,
			Type:     body.Type,
			Created:  time.Now().UTC(),
		})
	}

	_, err := bulk.Run()

	if err != nil {
		if mgo.IsDup(err) {
			return nil
		}
		return err
	}

	return nil
}

func main() {
	
	runtime.GOMAXPROCS(runtime.NumCPU())

	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		log.Fatal(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	// Drop Database
	if IsDrop {
		err = session.DB("test").DropDatabase()
		if err != nil {
			log.Fatal(err)
		}
	}

	// Collection Reviews
	reviewRepo := &ReviewRepo{Coll: session.DB("test").C("reviews")}

	//example link
	link := "http://www.yellowpages.com/newton-center-ma/mip/peets-coffee-tea-458789685?lid=1001447160391"

	//Init target and pool to scrape
	target := new(scraper.Target)
	target.BaseURL = strings.Trim(link, "? &")
	target.URLPool = make(map[string]bool)
	target.Parser = yp.ParseYP
	target.Saver = reviewRepo.mongoSaver
	target.URLPool, err = yp.MakeYPUrls(target.BaseURL)

	if err != nil {
		log.Fatal(err)
	}

	// run scraper
	scrapper := scrapper.NewScrapper()
	err = scrapper.Run(target)

	if err != nil {
		log.Fatal(err)
	}

	log.Println("Success!")
}
